﻿using System;
using System.Net;
using System.Xml;

namespace GameTutsAPI
{
    public class UserData
    {
        // Status of the API query
        public bool Success { get { return GetData("/root/success").toBool(); } }

        // UserID returned from API Query
        public int UserID { get { return GetData("/root/data/user_id").toInt(); } }

        // Username returned from API Query
        public string Username { get { return GetData("/root/data/username"); } }

        // Group which consists of ID, Title, Description, HTML Title
        public Group Group
        {
            get
            {
                return new Group
                {
                    ID = GetData("/root/data/group/group_id").toInt(),
                    Title = GetData("/root/data/group/group_title"),
                    Description = GetData("/root/data/group/group_description"),
                    Html_Title = GetData("/root/data/group/group_title_html")
                };
            }
        }

        // User Contact Information
        public ContactInfo ContactInfo
        {
            get
            {
                return new ContactInfo
                {
                    Homepage = GetData("/root/data/homepage"),
                    ICQ = GetData("/root/data/homepage"),
                    AIM = GetData("/root/data/aim"),
                    Yahoo = GetData("/root/data/yahoo"),
                    MSN = GetData("/root/data/msn"),
                    Skype = GetData("/root/data/skype")
                };
            }
        }

        // Media which consists of Avatar and Profile Picture
        public Media Media
        {
            get
            {
                return new Media
                {
                    Avatar = GetData("/root/data/media/avatar"),
                    ProfilePicture = GetData("/root/data/media/profile_pic")
                };
            }
        }

        // Total Post Count
        public int PostCount { get { return GetData("/root/data/posts/total_posts").toInt(); } }

        // Total thanked posts
        public int Thanks { get { return GetData("/root/data/posts/total_thanked").toInt(); } }

        // Total Profile Views
        public int ProfileViews { get { return GetData("/root/data/profile/total_thanked").toInt(); } }

        // Total Friends count
        public int Friends { get { return GetData("/root/data/profile/friends").toInt(); } }

        // The users signature
        public string Signature { get { return GetData("/root/data/profile/signature"); } }

        private string _XML;
        WebClient client = new WebClient();

        /// <summary>
        /// Initialize the GameTutsAPI without any parameters (Current logged in user)
        /// </summary>
        public UserData()
        {
            _XML = client.DownloadString("https://api.game-tuts.com/user?ret=xml");
            if (Success == false)
                throw new Exception(GetData("/root/data"));
        }

        /// <summary>
        /// Initialize the GameTutsAPI with a specific user id
        /// </summary>
        /// <param name="userID">The GameTuts user id you wish to lookup</param>
        public UserData(int userID)
        {
            _XML = client.DownloadString(string.Format("https://api.game-tuts.com/userid/{0}?ret=xml", userID));
            if (Success == false)
                throw new Exception(GetData("/root/data"));
        }

        /// <summary>
        /// Initialize the GametutsAPI with a specific username
        /// </summary>
        /// <param name="Username">The GameTuts username you wish to lookup</param>
        public UserData(string Username)
        {
            _XML = client.DownloadString(string.Format("https://api.game-tuts.com/user/{0}?ret=xml", Username));
            if (Success == false)
                throw new Exception(GetData("/root/data"));
        }

        private string GetData(string path)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(_XML);
            return doc.SelectSingleNode(path).InnerText;
        }
    }

    internal static class Extensions
    {
        /// <summary>
        /// Convert string to Boolean
        /// </summary>
        /// <param name="input">String to convert</param>
        /// <returns></returns>
        public static bool toBool(this string input) { return (input == "1" ? true : false) ; }

        /// <summary>
        /// Convert string to Int
        /// </summary>
        /// <param name="input">String to convert</param>
        /// <returns></returns>
        public static int toInt(this string input) { return Convert.ToInt32(input); }
    }
}

public class Group
{
    public int ID { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }
    public string Html_Title { get; set; }
}

public class ContactInfo
{
    public string Homepage { get; set; }
    public string ICQ { get; set; }
    public string AIM { get; set; }
    public string Yahoo { get; set; }
    public string MSN { get; set; }
    public string Skype { get; set; }
}

public class Media
{
    public string Avatar { get; set; }
    public string ProfilePicture { get; set; }
}